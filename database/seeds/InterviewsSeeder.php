<?php

use Illuminate\Database\Seeder;

class InterviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
                'date' => now(),
                'description' => Str::random(100),
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'date' => now(),
                'description' => Str::random(100),
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);
    }
}
