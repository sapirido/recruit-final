@extends('layouts.app')

@section('title','Edit Candidate')

@section('content')
    <h1>Edit Candidate <br> candidate id : {{$id}}</h1>
    @if(count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form method = "post" action = "{{action('CandidatesController@update',$candidate->id)}}"> <!-- which candidate I want to edit (by id) -->
        @csrf <!-- protect from csrf attack -->
        @method('PATCH')
        <div class="form-group">
            <label for = "name">Candidate name</label>
            <input type = "text" name = "name" class="form-control" value = {{$candidate->name}}>
        </div>
        <div class="form-group">
            <label for = "email">Candidate email</label>
            <input type = "text" name = "email" class="form-control" value = {{$candidate->email}}>
        </div>
        <div>
            <input class="btn btn-outline-primary" type = "submit" name = "submit" value = "Update candidate">
        </div>
    </form>
@endsection
