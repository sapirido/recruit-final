@extends('layouts.app')

@section('title','My Interviews list')

@section('content')
@if(Session::has('notallowed'))
<div class="alert alert-danger">
    {{Session::get('notallowed')}}
</div>
@endif
<div class="d-flex justify-content-between" style="padding-bottom:20px;">
    <h1 style="color:palevioletred;"> List of My Interviews</h1>
  <a href="{{url('/interviews/create')}}" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Add new Interview</a>
</div>
        @if($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{$message}}</p>
        </div>
        @endif
        @if(count($interviews)!=0)
        <table class="table table-striped">
            <tr>
                <th>Id</th><th>Date</th><th>Description</th><th>Candidate</th><th>Interviewer</th>
            </tr>
            <!-- table data -->
            @foreach($interviews as $interview)
                <tr>
                <td>{{ $interview->id}}</td>
                <td>{{ $interview->date}}</td>
                <td>{{ $interview->description}}</td>
                <td>
                    @if(isset($interview->candidate_id))
                    {{$interview->candidate->name}}
                    @else()
                    Assign Candidate
                @endif
                
                </td>
                <td>
                    @if(isset($interview->user_id))
                    {{$interview->user->name}} 
                    @else()
                    Assign Interviewer
                @endif
                
                </td>
               
                </tr>
            @endforeach
    </table>
    @else()
    <div>
        <div class="d-flex justify-content-center">
            <div class="d-flex flex-column">
                <div style="padding-bottom: 30px;"><h1>You don't have any interviews</h1></div> 
            </div> 
    @endif
@endsection