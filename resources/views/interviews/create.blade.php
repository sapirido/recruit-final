@extends('layouts.app')

@section('title','Create Interview')

@section('content')
<div class="d-flex justify-content-center">
    <div class="d-flex flex-column">
        <div style="padding-bottom: 30px;">
    <h1 style="color:palevioletred;">Create Interview</h1>
        </div>
    @if(count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form method = "post" action = "{{action('InterviewsController@store')}}"> <!-- transfer the data to store -->
        @csrf <!-- protect from csrf attack -->
        <div class="form-group ">
            <label for = "date">Date</label>
            <input type = "date" name = "date" class="form-control" placeholder = "Enter a Date">
        </div>
        <div class="form-group">
            <label for = "description">Description</label>
            <textarea type = "textarea" name = "description" class="form-control" rows="4" placeholder = "Enter Description"></textarea>
        </div>
        <div class="form-group row {{ $errors->has('candidate_id') ? ' has-error' : '' }}">
            <label for="candidate_id" class="col-md-4 col-form-label text-md-right">Candidate</label>
            <div class="col-md-6">
                <select style="width:200px;" class="form-control" name="candidate_id">
                <option value="" disabled selected hidden>Choose Candidate</option>
                @foreach($candidates as $candidate)
                    <option value="{{ $candidate->id }}">{{ $candidate->name }}</option>
                @endforeach
                </select>
            </div>
            @if ($errors->has('candidate_id'))
            <span class="help-block">
                <strong>{{ $errors->first('candidate_id') }}</strong>
            </span>
        @endif
        </div>
        <div  class="form-group row {{ $errors->has('user_id') ? ' has-error' : '' }}">
            <label for="user_id" class="col-md-4 col-form-label text-md-right">Interviewer</label>
            <div class="col-md-6">
                <select class="form-control" style="width:200px;" name="user_id">
                <option value="{{Auth::id()}}" disabled selected hidden> {{ Auth::user()->name }}</option>
                @foreach($users as $user)
                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
                </select>
            </div>
            @if ($errors->has('user_id'))
            <span class="help-block">
                <strong>{{ $errors->first('user_id') }}</strong>
            </span>
        @endif
        </div>
        <div class="d-flex justify-content-center" style="padding-top: 20px;">
            <input class="btn btn-outline-primary" type = "submit" name = "submit" value = "Create Interview">
        </div>
        
    </form>
  </div>
</div>
@endsection
