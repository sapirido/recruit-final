<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use App\Candidate;
use App\User;
use App\Interview;
use App\Status;
use App\Department;
use Illuminate\Support\Facades\Auth;

//full name is "APP\Http\Controllers\InterviewsController";
class InterviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){
        $interviews = Interview::all();
        return view('interviews.index',compact('interviews')); 
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

     
    public function create()
    {
        Gate::authorize('add-interview');
        $candidates = Candidate::all();
        $users = User::all();
        return view('interviews.create',compact('candidates','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'date' => 'required',
            'description' => 'required'
        ]);
        $interview = new Interview();
        $interview->create($request->all());
        return redirect('interviews');   
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function myInterviews()
    {        
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $interviews = $user->interviews;
        $users = User::all();

        return view('interviews.myinterviews',compact('interviews','users'));
    }
    

}

