<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    protected $fillable = ['date','description','candidate_id','user_id']; 
    
    public function candidate(){ 
        return $this->belongsTo('App\Candidate','candidate_id'); //each interview has only one candidate
    }
    public function user(){
        return $this->belongsTo('App\User', 'user_id');   //each interview has only one user
    }

}
